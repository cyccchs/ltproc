#include "ltproc.hpp"

using namespace std;
using namespace cv;

int main()
{
    ltproc A;
    char kbin;
    
/*
    int d=1;
    int sigma_color=150, sigma_space=150;
    int dilate_iter = 1;
    int erode_iter = 1;
    int th1 = 50, th2 = 150;
    
    namedWindow("canny");
    createTrackbar("th1", "canny", &th1, 500);
    createTrackbar("th2", "canny", &th2, 500);
    namedWindow("blur");
    createTrackbar("radius", "blur", &d, 500);
    createTrackbar("sigma_color", "blur", &sigma_color, 500);
    createTrackbar("sigma_space", "blur", &sigma_space, 500);
    namedWindow("erode");
    createTrackbar("iter", "erode", &erode_iter, 10);
    namedWindow("dilate");
    createTrackbar("iter", "dilate", &dilate_iter, 10);
*/
    A.set_cam(1);
    A.set_window(640, 480);
    A.set_prune_distance(30);
    while(1)
    {
        A.find_contour();
        A.find_side_point();
        A.prune_by_distance();
        A.show_result();
        kbin = waitKey(1);
        if(kbin == 27)
            break;
    }
}
