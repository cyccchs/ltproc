#include <iostream>
#include <unistd.h>
#include <opencv2/opencv.hpp>

class lt_coor
{
    public:
        int x;
        int y;
        int num;
        unsigned int peer;
};


class ltproc
{
    public:
        ltproc();
        void set_cam(int camIndex);
        void set_window(int height, int width);
        void set_lane_width(int width);
        void set_prune_distance(int prune_dis);
        void find_contour();
        void find_side_point();
        void prune_by_distance();
        void find_mid_point();
        void show_result();
        void show_gray_img();
        void show_canny_img();
        void show_erode_img();
        void show_blur_img();
        void show_dilate_img();
        ~ltproc();


    private:
        cv::Mat mask[7];
        cv::VideoCapture lt_cam;
        int lt_lane_width;
        int lt_prune_dis;
        int rsl_width, rsl_height;
        cv::Mat lt_frame;
        std::vector<lt_coor> sidePt;

        int lt_camIndex;

        cv::Mat gray;
        cv::Mat can;
        cv::Mat ero;
        cv::Mat blur;
        cv::Mat dil;
        int d;
        int sigmaColor;
        int sigmaSpace;
        int th1;
        int th2;
        int dilate_iter;
        int erode_iter;
};

float calc_dis(lt_coor a, lt_coor b);
