#include "ltproc.hpp"

ltproc::ltproc():   lt_lane_width (60), 
                    lt_prune_dis(30), 
                    d(5), 
                    sigmaColor(150), 
                    sigmaSpace(150), 
                    th1(50), 
                    th2(150), 
                    dilate_iter(1), 
                    erode_iter(1)
{
}

void ltproc::set_cam(int camIndex)
{
    lt_camIndex = camIndex;
    lt_cam = cv::VideoCapture(lt_camIndex);
}

void ltproc::set_window(int width, int height)
{
    rsl_width = width;
    rsl_height = height;
    lt_cam.set(CV_CAP_PROP_FRAME_WIDTH, rsl_width);
    lt_cam.set(CV_CAP_PROP_FRAME_HEIGHT, rsl_height);
    for(int i=0;i<7;i++)
    {
        mask[i].create(rsl_height, rsl_width, CV_8U);
        mask[i] = cv::Scalar(0);
        cv::ellipse(mask[i],
                    cv::Point(mask[i].cols/2,mask[i].rows), 
                    cv::Size(rsl_width/2, rsl_height-(int)(rsl_height/6*i)), 
                    0, 0, 360, 
                    cv::Scalar(255), 
                    1, 8, 0);
    }
}

void ltproc::set_lane_width(int width)
{
    lt_lane_width = width;
}
void ltproc::set_prune_distance(int prune_dis)
{
    lt_prune_dis = prune_dis;
}

void ltproc::find_contour()
{
    lt_cam.read(lt_frame);
    cv::erode(lt_frame, ero, cv::Mat(), cv::Point(-1,-1), erode_iter);
    cv::bilateralFilter(ero, blur, d, sigmaColor, sigmaSpace);
    cv::cvtColor(blur, gray, cv::COLOR_BGR2GRAY);
    cv::Canny(gray, can, th1, th2, 3);
    cv::dilate(can, dil, cv::Mat(), cv::Point(-1,-1), dilate_iter);
}

void ltproc::find_side_point()
{
    sidePt.clear();
    lt_coor tmp;
    cv::Mat buf;
    bool found = false;
    for(int i=0;i<7;i++)
    {
        cv::bitwise_and(dil, mask[i], buf);

        for(int j=0; j<dil.rows; j++)
        {
            for(int k=0; k<dil.cols; k++)
            {
                if(buf.at<uchar>(j,k) > 0)
                {
                    found = true;
                    tmp.x = k;
                    tmp.y = j;
                    tmp.num = i;
                    sidePt.push_back(tmp);
                }
            }
        }
    }
    if(!found)
    {
        tmp.x = rsl_width/2;
        tmp.y = rsl_height/2;
        for(int i=0;i<7;i++){
            sidePt.push_back(tmp);
        }
    }
}

void ltproc::prune_by_distance()
{
    bool pruned = false;
    while(1)
    {
        pruned = false;
        for(int i=0;i<(int)sidePt.size();i++)
        {
            for(int j=i+1;j<(int)sidePt.size()-1;j++)
            {
                if(calc_dis(sidePt[i],sidePt[j]) < lt_prune_dis)
                {
                    sidePt.erase(sidePt.begin()+j);
                    pruned = true;
                }	
            }
            for(int j=i-1;j>=0;j--)
            {
                if(calc_dis(sidePt[i],sidePt[j]) < lt_prune_dis)
                {
                    sidePt.erase(sidePt.begin()+j);
                    pruned = true;
                }	
            }
        }
        if(!pruned)
            break;
    }

    std::vector<lt_coor> new_sidePt;

    for(int i=0;i<7;i++)
    {
        int ptA = 0, ptB = 0;
        float widthDiff, currentDiff;
        std::vector<lt_coor> tmp;
        std::vector<int> ptbuf;
        bool first_time = true;
        
        for(int j=0;j<(int)sidePt.size();j++)
        {
            if(sidePt[j].num == i)
            {
                tmp.push_back(sidePt[j]);
                ptbuf.push_back(j);
            }
        }

        for(int j=0;j<(int)tmp.size();j++)
        {
            for(int k=0;k<(int)tmp.size();k++)
            {
                widthDiff = fabs(calc_dis(tmp[j], tmp[k])-lt_lane_width);
                if(j!=k && (first_time || widthDiff < currentDiff))
                {
                    currentDiff = widthDiff;
                    ptA = ptbuf[j];
                    ptB = ptbuf[k];
                    first_time = false;
                }
            }
        }
        new_sidePt.push_back(sidePt[ptA]);
        new_sidePt.push_back(sidePt[ptB]);

    }
    sidePt.clear();
    sidePt = new_sidePt;
    new_sidePt.clear();
}

void ltproc::show_result()
{
    cv::Mat result(rsl_height, rsl_width, CV_8UC3);
    result = cv::Scalar(0);
    for(int i=0;i<(int)sidePt.size();i++)
    {
        cv::circle( result, 
                    cv::Point(sidePt[i].x,sidePt[i].y), 
                    3, 
                    cv::Scalar(0,0,255),5);
    }
    std::cout<<"side point's size:"<<sidePt.size()<<std::endl;
    cv::imshow("result", result);
    cv::imshow("origin", lt_frame);
}

void ltproc::show_gray_img()
{
    cv::imshow("gray", gray);
}
void ltproc::show_canny_img()
{
    cv::imshow("canny", can);
}
void ltproc::show_erode_img()
{
    cv::imshow("erode", ero);
}
void ltproc::show_blur_img()
{
    cv::imshow("blur", blur);
}
void ltproc::show_dilate_img()
{
    cv::imshow("dilate", dil);
}

ltproc::~ltproc(){}

float calc_dis(lt_coor a, lt_coor b)
{
    float distance = sqrt(pow(a.x-b.x,2)+pow(a.y-b.y,2));
    return distance; 
}
